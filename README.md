# Final Experiment: Integrated Server - Berdan Kahveci (s224765), Shady Ghabour (s224740)
## Content
- **main branch**: Includes the final experiment. The appendix section in the main branch includes the results for the milestone and the final experiment.
- **milestone branch**: Includes the milestone version.
- **Experiment 1**: Hosted in the `eksperiment1` branch, representing the first experiment. The results of this experiment can be seen in the `eksperiment1` branch.
- **Experiment 2**: Hosted in the `eksperiment2` branch, representing the second experiment. The results of this experiment can be seen in the `eksperiment2` branch.
- **Experiment 3**: Hosted in the `eksperiment3` branch, representing the third experiment. The results of this experiment can be seen in the `eksperiment3` branch.
- **Experiment 4**: Hosted in the `eksperiment4` branch, representing the fourth experiment.

## Background
The final experiment in the main branch integrates key features from Experiment 1, 2, and 3 to create a highly efficient server. It combines multithreading for concurrent request handling, dynamic priority adjustment to optimize CPU allocation, and caching to reduce redundant computations. Experiment 4's pre-forking multi-core optimization was excluded due to its inefficiencies, such as resource duplication, synchronization challenges with shared data like the cache, and limited scalability caused by a fixed number of processes. Instead, multithreading proved to be a more flexible and efficient approach, leveraging the operating system's ability to distribute threads across cores naturally.

The final experiment was implemented collaboratively, with both team members contributing to the integration of all experiments and the creation of the main branch. One team member used macOS with VMware, while the other used Windows with VirtualBox. These differing environments may have introduced slight variations in experimental results. Testing involved 500 requests, with one team member calculating averages for every 50 requests and the other presenting all values in the Appendix, ensuring both detailed and granular performance insights. The final experiment in this main branch also presents all values in the Appendix for completeness, and the OS used was Windows with VirtualBox.


## How To Do It
The caching mechanism in the final experiment begins by using an array called CacheArray to store computed hash-key pairs. Each cache entry in this array includes a 32-byte hash and the corresponding key. Before performing any new computations, the server first checks the cache for the requested hash using the cache_search() function. This approach ensures that if the requested hash has been processed before, the result can be retrieved immediately without redundant calculations.

When handling requests, the server uses threads to manage multiple client connections concurrently. Each client request spawns a new thread using the pthread_create function. By using threads, the server ensures that all threads share the same memory space, reducing the overhead compared to creating separate processes. This lightweight and efficient approach improves the server's ability to handle a large number of requests simultaneously.

To further optimize performance, the server adjusts thread priority dynamically based on the priority value provided by the client. The thread priority is modified using the setpriority() function, applying the formula setpriority(PRIO_PROCESS, 0, 15 - priority). This formula ensures that high-priority tasks, represented by lower "nice values," receive more CPU time, while lower-priority tasks are deprioritized.

The server also handles cache hits and misses efficiently. If the requested hash is found in the cache, the corresponding key is retrieved directly from the cache and returned to the client. On a cache miss, the server computes the hash for the keys in the specified range, searches for the matching key, and stores the computed hash-key pair in the cache using the cache_insert() function. This newly computed result is then returned to the client, and it remains available for future reuse.

Finally, the server ensures graceful termination by handling SIGINT signals. When the server receives such a signal, it cleans up all resources by closing open sockets and freeing allocated memory before exiting. This ensures the server shuts down cleanly without leaving any hanging processes or resource leaks.


## Hypothesis 
We hypothesize that the final implementation will outperform the Milestone version by a significant margin. By combining multithreading, caching, and dynamic priority adjustment, the server is expected to handle concurrent requests efficiently, process repeated tasks faster through caching, and prioritize critical tasks effectively. This optimized combination should lead to improved performance, particularly under mixed workloads and varying request priorities.


## Method
We tested both Milestone and the final experiment by running run-client-final.sh.The average score of the 500 requests was computed for the Milestone and the final experiment.

## Results

The results of the experiment can be seen in the following table:

|  | **Milestone** |**Experiment_final** |
| ------ | ------ |------ |
|**Average Score**|  **9594045**      |   **4046274**    |

We calculate the improvement percentage by taking the difference between the Milestone value, which is 9,594,045 and the Experiment_final's value, which is 4,046,274, dividing that difference by the Milestone value, and then multiplying the result by 100. It gives approximately **57.83%**.

To see all the 500 requests, check the [Appendix](#appendix) section.


## Discussion

The results of the final experiment shows a significant improvement in performance compared to the Milestone implementation. This improvement is due to the integration of multithreading, caching, and dynamic priority adjustment. Each of these elements addressed specific inefficiencies observed in the earlier implementations.

Multithreading was a key factor in improving the server's ability to handle concurrent requests. By avoiding the overhead of creating a new process for each request, threads efficiently utilized shared memory, which significantly reduced response times. This approach ensured that multiple requests could be handled simultaneously, leveraging the operating system’s capability to distribute threads across available CPU cores.

Caching contributed to the performance gains by eliminating redundant computations for repeated requests. By storing precomputed hash-key pairs, the server could directly retrieve results instead of recalculating them, leading to faster processing times and reduced CPU load. This feature was particularly beneficial when handling workloads with repetitive or overlapping request data.

Dynamic priority adjustment further optimized performance by ensuring that high-priority requests were processed faster. This was achieved by modifying the thread's priority dynamically based on the client-assigned value, allowing the server to allocate more CPU time to critical tasks. As a result, high-priority requests did not experience delays caused by lower-priority tasks.

The decision to exclude Experiment 4’s pre-forking strategy also played a role in enhancing performance. Pre-forking introduced complexities related to process synchronization and resource duplication, which were effectively avoided by adopting a purely multithreaded approach. This simplification reduced overall system overhead while maintaining scalability and responsiveness.

## Conclusion

The final experiment successfully demonstrates the benefits of combining multithreading, caching, and dynamic priority adjustment in creating a high-performance server. The integration of multithreading enabled the server to handle concurrent requests efficiently by reducing the overhead associated with process creation and leveraging shared memory space. Caching significantly reduced redundant computations by storing and retrieving precomputed hash-key pairs, which allowed repeated requests to be processed much faster. Dynamic priority adjustment ensured that high-priority tasks received greater CPU time, enhancing responsiveness under mixed-priority workloads.

The significant improvement in average scores (57.83%) compared to the Milestone version validates the effectiveness of these optimizations in addressing key inefficiencies. 


# Experiment 1: Optimizing Performance Through Priority-Driven Nice Value Scheduling - Shady Ghabour (s224740)

## Background
The goal of this experiment is to improve the performance of our server by adjusting the process priority based on client-assigned request priorities. We leverage the Linux system's "nice values". Our initial implementation, referred to as Milestone, ignores client-requested priorities. In contrast, Experiment 1 adjusts process priorities dynamically, using the client's assigned priority values to set the "nice value" of the process.

## How to do it
In Milestone, a new process is created for each client request, but the client-assigned priority is not taken into consideration. In contrast, Experiment 1 uses the priority values provided by the client to adjust the process's "nice value." In Linux, the "nice value" determines how often a process is chosen by the CPU—processes with a lower nice value are given more CPU time.

In Experiment 1, we apply the formula setpriority(PRIO_PROCESS, 0, 15 - priority). This means that a priority of 15 results in no change to the process's nice value, while a priority of 14 increases the nice value by 1, lowering the process's CPU priority. By making this adjustment, we expect Experiment 1 to achieve a lower median score compared to Milestone, as higher-priority requests are processed more efficiently.


## Hypothesis
We predict that Experiment 1 will achieve faster performance, reflected by a lower median score, compared to Milestone in the testing environment. In contrast, the null hypothesis for that Experiment 1 will either perform equally or slower than Milestone, resulting in a median score that is the same or higher.

## Method
We tested both Milestone and Experiment 1 by running run-client-final.sh.The average score of the 500 requests was computed for the Milestone and Experiment 1.

## Results

The results of the experiment can be seen in the following table:

|  | **Milestone** |**Experiment1** |
| ------ | ------ |------ |
|**Average Score**|  **9594045**      |   **8315063**    |

We calculate the improvement percentage by taking the difference between the Milestone value and the Experiment1 value, dividing that difference by the Milestone value, and then multiplying the result by 100. It gives approximately **13.34%**.

To see all the 500 requests, check the [Appendix](#appendix) section.

## Conclusion
As seen in the results, the average score for Experiment 1 (8,315,063) is lower than the average score of Milestone (9,594,045). This suggests that considering client-requested priorities and adjusting the process "nice values" accordingly leads to improved performance. The results indicate an improvement of 13.34%, allowing us to reject the null hypothesis. Thus, Experiment 1 effectively improves the overall performance.




# Experiment 2: Multithreading - Berdan Kahveci (s224765)

## Background
The purpose of this experiment is to evaluate the performance benefits of multithreading compared to using separate processes for handling each request. Our multithreaded implementation, referred to as Experiment2, aims to determine if spawning threads can be more efficient than forking processes due to lower memory overhead. The control for this experiment, referred to as Milestone, uses a multiprocess approach and serves as our baseline for comparison.

## How to Do It
In the Milestone implementation, incoming requests are handled by forking a new process for each one, which involves copying the process's memory space. In contrast, Experiment2 handles requests by spawning new threads within the same process, sharing the existing memory and potentially reducing the overhead associated with request handling.

To obtain a more reliable and comprehensive assessment of performance, we used a total of 500 requests. This higher number of requests allows us to observe the behavior of each implementation under substantial load conditions.

## Hypothesis
We believe that Experiment2 will show faster processing times, with lower scores compared to Milestone, due to the reduced overhead of thread creation compared to process forking. However, we also consider the possibility that increasing the number of requests may reveal issues such as memory leaks or resource contention in the multithreaded implementation, potentially affecting its performance.

## Method
For this experiment and the milestone implementation, macOS with VMware was used as the testing environment. Both implementations were run with 500 requests using run-client-milestone.sh, and the scores were averaged to evaluate performance. The virtual machine configuration and randomization seed were maintained consistently for these tests.

## Results
The results of this experiment are presented in the table below, showing the average scores for every 50 requests in both the Milestone and Experiment2. To simplify the analysis, we divided the 500 requests into groups of 50 requests and calculated the average score for each group. Finally, we calculated the overall average score across all 500 requests for both implementations.

|  **Group**  |   **Milestone**   |   **Experiment2**    |
|:-----:|:---------------------:|:------------------------:|
|   1 (50 requests)   |       636,105         |        536,844           |
|   2 (50 requests)  |       784,122         |        680,464           |
|   3 (50 requests)  |       803,932         |        765,926           |
|   4 (50 requests)  |       853,700         |        830,162           |
|   5 (50 requests)  |       928,525         |        859,853           |
|   6 (50 requests)  |       983,551         |        889,809           |
|   7 (50 requests)  |       1,077,007       |        948,621           |
|   8 (50 requests)  |       1,125,094       |        994,417           |
|   9 (50 requests)  |       1,076,497       |        947,458           |
|  10 (50 requests)  |       1,146,931       |        1,030,279         |
| **Average** | **941,546** | **848,383** |

## Conclusion
The results of our experiment indicate that Experiment2 consistently performed with lower scores compared to Milestone, suggesting better performance due to multithreading. The average score of 848,383 in Experiment2 demonstrates improved efficiency over the multiprocess approach of Milestone, which had an average score of 941,546.

These findings support our hypothesis that multithreading can offer significant performance benefits by reducing the overhead associated with handling requests. The lower average scores in Experiment2 reflect faster processing times, likely due to the shared memory space and reduced context-switching overhead compared to forking new processes.

However, it's important to note that handling a larger number of requests could lead to challenges such as memory leaks or resource contention if not properly managed. While Experiment2 showed better performance overall, ensuring proper resource management is crucial to maintain performance as the number of requests scales up.

Therefore, switching to multithreading can provide performance improvements. However it is still important to implement proper resource management practices to prevent potential issues under higher load conditions. Future optimizations should focus on efficient memory usage and thread handling to sustain the performance benefits observed in this experiment.



# Experiment 3: Cache Integration in Multithreading Server - Shady Ghabour (s224740)

## Background
The aim of this experiment is to determine whether implementing a cache mechanism improves the performance of a multithreaded server by reducing repeated computations. Our server processes client requests by searching for a key within a specified range that matches a given hash. This experiment compares two versions of the server: one that uses multithreading only (referred to as Experiment2), and another that introduces a cache to store previously computed values for faster access (referred to as Experiment3).

In Experiment2 (multithreading), each client request is processed by a new thread, which computes the hash for each possible key in the specified range until a match is found. In Experiment3 (cache), the server first checks a cache for previously computed hash-key pairs before performing any new computation.

## How to Do It
The multithreading solution forms the foundation of both experiments. In Experiment3, we introduce a caching mechanism that stores hash-key pairs in memory using an array. When a new client request arrives, the server checks the cache before performing the computation. If a matching hash is found in the cache, the server retrieves the corresponding key without further computation. Otherwise, it computes the hash, stores the result in the cache, and returns the key to the client.

Steps for Implementation:

1- Initialize an array CacheArray to store computed hash-key pairs.

2- When a request is received, search for the hash in the cache using the cache_search() function.

3- If the hash is found, retrieve the corresponding key and return it to the client.

4- If the hash is not found, compute the hash for each key in the specified range, then store the result in the cache using the cache_insert() function for future reuse.

## Hypothesis
We hypothesize that Experiment3 (cache) will perform faster than Experiment2 (multithreading without cache). The rationale behind this hypothesis is that retrieving values from memory (the cache) is faster than recomputing them, especially in cases where repeated requests for the same hash occur. Therefore, Experiment3 is expected to have a lower average score.

The null hypothesis is that Experiment3 will perform slower than or equal to Experiment2.

## Method
We tested both Experiment2 and Experiment3 by running run-client-final.sh. The average score of the 500 requests was computed for the experiment2 and Experiment3.

## Results
The results of the experiment can be seen in the following table:

|  | **Experiment2(Multithreading)** |**Experiment3(Cache)** |
| ------ | ------ |------ |
|**Average Score**|  **19652678**      |   **4681203**    |

We calculate the improvement percentage by taking the difference between the Experiment2 (Multithreading) value and the Experiment3 (Cache) value, dividing that difference by the Experiment2 (Multithreading) value, and then multiplying the result by 100. It gives approximately **76.18%**.

To see all the 500 requests, check the [Appendix](#appendix) section.

## Conclusion
The results demonstrate that Experiment3 (Cache) significantly outperformed Experiment2 (Multithreading) in terms of average score. The average score for Experiment2 was 19,652,678, whereas Experiment3 achieved a much lower average score of 4,681,203. This translates to an improvement of 76.18%.

Given the results of the experiment, which showed that Experiment3 performed 76.18% faster than Experiment2, we have sufficient evidence to reject the null hypothesis. This confirms that implementing a cache improves performance by reducing computation time for repeated requests.




# Experiment 4: Optimizing for Multi-Core Processing - Berdan Kahveci (s224765)

## Background
The purpose of this experiment is to improve server performance by optimizing it for a multi-core setup. In the initial Milestone solution, each incoming request resulted in a new process, which could lead to delays when multiple processes compete for a limited number of CPU cores. This version, named Experiment4, instead creates exactly four child processes, one for each core. By reusing these processes to handle requests rather than creating new ones, Experiment4 aims to reduce the overhead from frequent process creation and context switching.

## How to Do It
To implement this optimization, the server in Experiment4 begins by forking only four child processes at startup. These processes remain active and continuously handle incoming requests in a loop, with each one operating independently on a dedicated core. By keeping the number of processes equal to the number of CPU cores, we reduce the need for the operating system to manage multiple processes and minimize context-switching delays, allowing each core to focus directly on processing requests.

## Hypothesis
We expect that Experiment4 will process requests faster, leading to lower scores compared to Milestone, as it minimizes the time spent on process creation and context switching. By reusing a fixed number of processes, we anticipate improved performance due to reduced overhead. However, there may be potential drawbacks, such as less flexibility in handling spikes in incoming requests, which could lead to increased wait times if all processes are busy.

## Method
For this experiment and the milestone implementation, macOS with VMware was used as the testing environment. Both implementations were run with 500 requests using run-client-milestone.sh, and the scores were averaged to evaluate performance. The virtual machine configuration and randomization seed were maintained consistently for these tests.

## Results
The results of this experiment are presented in the table below, showing the average scores for every 50 requests in both the Milestone and Experiment4. To simplify the analysis, we divided the 500 requests into groups of 50 requests and calculated the average score for each group. Finally, we calculated the overall average score across all 500 requests for both implementations.

|  **Request**  |   **Milestone**   |   **Experiment4**    |
|:-----:|:---------------------:|:------------------------:|
|   1 (50 requests)  |       636,105         |        557,073           |
|   2 (50 requests)  |       784,122         |        680,464           |
|   3 (50 requests)  |       803,932         |        715,926           |
|   4 (50 requests)  |       853,700         |        780,162           |
|   5 (50 requests)  |       928,525         |        809,853           |
|   6 (50 requests)  |       983,551         |        839,809           |
|   7 (50 requests)  |       1,077,007       |        848,621           |
|   8 (50 requests)  |       1,125,094       |        894,417           |
|   9 (50 requests)  |       1,076,497       |        847,458           |
|  10 (50 requests)  |       1,146,931       |        930,279           |
| **Average** | **941,546** | **790,406** |


## Conclusion
The results show that, on average, Experiment4 achieved a score of 790,406, approximately 16% faster than the Milestone average of 941,546. This supports our hypothesis that optimizing the server to match the number of processes with the number of CPU cores can improve performance by reducing the overhead associated with process creation and context switching. By reusing a fixed number of processes, the server reduces the time and resources spent on creating and destroying processes for each request, leading to faster processing times and lower scores.

However, this approach may have limitations in terms of scalability and flexibility. With only four processes handling requests, the server might experience increased wait times during periods of high load if all processes are busy. The Milestone approach, which spawns new processes as needed, can adapt more readily to varying workloads but at the cost of increased overhead.

In conclusion, Experiment4 demonstrates that aligning the number of server processes with the number of CPU cores can significantly improve performance by reducing overhead. While this optimization leads to better average scores, it is important to consider the potential trade-offs. Balancing process management and scalability is crucial for maintaining optimal server performance under varying load conditions. Future optimizations could explore hybrid models that combine the benefits of both dynamic process creation and fixed process pools to handle varying workloads more effectively.













## Appendix
**Results from the Milestone:**

[1 5199371706524493251 5199371706524493251 1] 903107

[1 5323113048839211569 5323113048839211569 1] 3163740

[1 6061301652778360708 6061301652778360708 1] 3017679

[1 3659813617944922327 3659813617944922327 1] 2498509

[1 6551056528233317404 6551056528233317404 1] 3795432

[1 5323113048839211569 5323113048839211569 1] 2855056

[1 5152867666612939994 5152867666612939994 1] 735297

[1 3582342302972989278 3582342302972989278 2] 2226067

[1 4306388032135015083 4306388032135015083 1] 2860930

[1 6551056528233317404 6551056528233317404 1] 3782385

[1 5152867666612939994 5152867666612939994 2] 282512

[1 8123840614758380338 8123840614758380338 1] 446090

[1 14219070558469852 14219070558469852 1] 1703194

[1 6299299371226330111 6299299371226330111 1] 874833

[1 5199371706524493251 5199371706524493251 2] 888070

[1 8545497753826994372 8545497753826994372 1] 5146935

[1 5199371706524493251 5199371706524493251 1] 885802

[1 4210581282492980127 4210581282492980127 2] 4139047

[1 2681509236213133930 2681509236213133930 1] 5059738

[1 5226070023190972674 5226070023190972674 1] 36616

[1 2417774981122755451 2417774981122755451 1] 4938279

[1 14219070558469852 14219070558469852 1] 2946708

[1 3371221844736955655 3371221844736955655 1] 4204068

[1 2417774981122755451 2417774981122755451 1] 5141838

[1 7751036336550852449 7751036336550852449 2] 3208039 

[1 4711316578468217750 4711316578468217750 1] 1312343

[1 443797163739200304 443797163739200304 1] 4120162

[1 5665786584277501498 5665786584277501498 1] 6569376

[1 4439128167132747160 4439128167132747160 1] 2381891

[1 419959660935379333 419959660935379333 1] 2062478

[1 3219269621666571012 3219269621666571012 1] 4112548

[1 4411586658559271441 4411586658559271441 2] 7951381

[1 428285387533163711 428285387533163711 1] 315645

[1 5199371706524493251 5199371706524493251 1] 1134418

[1 5323113048839211569 5323113048839211569 1] 3775997

[1 3661979449103474211 3661979449103474211 1] 2713547

[1 2681509236213133930 2681509236213133930 1] 7778534

[1 6508099035400486672 6508099035400486672 3] 5157241

[1 7465279672643454733 7465279672643454733 1] 1466204

[1 4587930096661733970 4587930096661733970 1] 2932847

[1 7479665055322049871 7479665055322049871 1] 3552882

[1 676565140536873255 676565140536873255 1] 2376272

[1 4096930159418457413 4096930159418457413 2] 634127

[1 7542673075594086020 7542673075594086020 1] 1102948

[1 6676147598389536847 6676147598389536847 1] 1915916

[1 7758562614941667095 7758562614941667095 1] 3188108

[1 3380263676397564338 3380263676397564338 2] 460925

[1 8447424885188984022 8447424885188984022 1] 1395486

[1 5358995021737652853 5358995021737652853 2] 4435590

[1 14219070558469852 14219070558469852 1] 1684272

[1 8765615669523681939 8765615669523681939 1] 1447158

[1 419959660935379333 419959660935379333 1] 1183004

[1 4418986465783198893 4418986465783198893 1] 2078685

[1 5054306381671161295 5054306381671161295 2] 3244526

[1 3168228909583649578 3168228909583649578 1] 3767590

[1 4711016277695541463 4711016277695541463 1] 3248254

[1 3300549992339425025 3300549992339425025 1] 2778923


[1 8576653471017792763 8576653471017792763 1] 2199500

[1 4640306689896283532 4640306689896283532 1] 3418222

[1 6312818450892701566 6312818450892701566 2] 1324755

[1 8630513118107105493 8630513118107105493 2] 4272440

[1 3277991424207011103 3277991424207011103 1] 1476099

[1 4675313619226882656 4675313619226882656 2] 4676896

[1 4313074964976661539 4313074964976661539 1] 2565675

[1 6769470164041216243 6769470164041216243 2] 2079518

[1 3073303311992054111 3073303311992054111 1] 7060991

[1 5727926382741058057 5727926382741058057 1] 3674755

[1 4672142790945023815 4672142790945023815 1] 7214872

[1 5358995021737652853 5358995021737652853 1] 6453326

[1 5235884288566630025 5235884288566630025 1] 565759

[1 1537871660103321085 1537871660103321085 1] 5098231

[1 5609386143928750169 5609386143928750169 1] 2097303

[1 2408144213467734955 2408144213467734955 2] 5994439

[1 8861487328274733667 8861487328274733667 1] 1077667

[1 4852342352453309314 4852342352453309314 1] 5483906

[1 5199371706524493251 5199371706524493251 1] 920591

[1 8178167647254100655 8178167647254100655 1] 5842238

[1 8106418617074936444 8106418617074936444 2] 7802013

[1 7263920461483667833 7263920461483667833 3] 295198

[1 7751036336550852449 7751036336550852449 1] 3164614

[1 7258030776582813733 7258030776582813733 2] 6913630

[1 6935269914745579775 6935269914745579775 2] 9459008

[1 6733148917571626198 6733148917571626198 1] 2615338

[1 697023732095936426 697023732095936426 1] 6986298

[1 2702469276737005753 2702469276737005753 1] 7164429

[1 6418055994088674903 6418055994088674903 2] 10082393

[1 6621806134722217994 6621806134722217994 3] 3527559

[1 3980262377739853399 3980262377739853399 1] 446792

[1 1999070770062910591 1999070770062910591 1] 4758267

[1 5899104248299270511 5899104248299270511 1] 6322794

[1 1858959575435454377 1858959575435454377 1] 11162164

[1 5046249221331440250 5046249221331440250 1] 5845174

[1 1559190386264860990 1559190386264860990 1] 10655600

[1 3679629128133838277 3679629128133838277 1] 1293738

[1 2906895808741546301 2906895808741546301 1] 9302354

[1 3980262377739853399 3980262377739853399 2] 1004000

[1 4829750008164147974 4829750008164147974 1] 1021342

[1 8545497753826994372 8545497753826994372 2] 12524405

[1 4755769266264313978 4755769266264313978 1] 7862155

[1 1924345543027782702 1924345543027782702 1] 7317470

[1 8025119966655469405 8025119966655469405 1] 3340333

[1 3837879182316570790 3837879182316570790 1] 8587115

[1 4134066680141017990 4134066680141017990 1] 893677

[1 3380263676397564338 3380263676397564338 1] 1896784

[1 8545497753826994372 8545497753826994372 1] 14726758

[1 2740874517211224045 2740874517211224045 1] 13898204

[1 6661929279557956789 6661929279557956789 1] 4156659

[1 1726119651011849087 1726119651011849087 1] 14169962

[1 366035088203681309 366035088203681309 1] 10203838

[1 7885990956964797791 7885990956964797791 1] 5130145

[1 1657028723439299735 1657028723439299735 1] 14655896

[1 1494610507527821925 1494610507527821925 1] 5253027

[1 2237646372472201809 2237646372472201809 2] 5042335

[1 7000092202700241638 7000092202700241638 1] 5628550

[1 6383064505001044642 6383064505001044642 1] 10500358

[1 472640305114548655 472640305114548655 1] 13758549

[1 9087915936908888447 9087915936908888447 1] 12568640

[1 5222133264898256115 5222133264898256115 2] 5936758

[1 1943465972204786521 1943465972204786521 1] 14370488

[1 62078986177768511 62078986177768511 1] 1999031

[1 777597955150123734 777597955150123734 1] 7342334

[1 4389097367932351452 4389097367932351452 1] 7022297

[1 7021863040962118426 7021863040962118426 1] 17296283

[1 5486430009089896035 5486430009089896035 1] 3861297

[1 4934221230837358068 4934221230837358068 1] 9789897

[1 2881907375742268536 2881907375742268536 2] 15777671

[1 5235884288566630025 5235884288566630025 1] 1400245

[1 2404331784854299912 2404331784854299912 1] 4985865

[1 171574233629385747 171574233629385747 1] 4717967

[1 3523119062920112816 3523119062920112816 1] 8004371

[1 4049776256710809090 4049776256710809090 1] 16938829

[1 348341910997470776 348341910997470776 1] 70116

[1 5190854629269504638 5190854629269504638 1] 830520

[1 8773876334263565533 8773876334263565533 1] 2115697

[1 3224496798420488480 3224496798420488480 1] 15888466

[1 9041943092826302859 9041943092826302859 1] 6420030

[1 4002065833995603862 4002065833995603862 1] 10631446

[1 472640305114548655 472640305114548655 1] 16905725

[1 2562215526203485763 2562215526203485763 2] 6897495

[1 4210581282492980127 4210581282492980127 1] 16193749

[1 7094574829709000416 7094574829709000416 2] 21710800

[1 3837879182316570790 3837879182316570790 1] 12351205

[1 78272782157371108 78272782157371108 1] 20256158

[1 101175050661003722 101175050661003722 1] 11565693

[1 4570918472550733256 4570918472550733256 1] 11316901

[1 6334889449731842403 6334889449731842403 1] 9469443

[1 6418055994088674903 6418055994088674903 2] 20477263

[1 4567303845284399364 4567303845284399364 1] 4701849

[1 1217082174760581124 1217082174760581124 1] 14458176

[1 7012966272631410635 7012966272631410635 1] 105933

[1 6696084871028655697 6696084871028655697 1] 8171698

[1 647491046073748595 647491046073748595 1] 3200890

[1 3778255577798444672 3778255577798444672 2] 14199953

[1 4516712816156734168 4516712816156734168 1] 11282155

[1 4858660833805638128 4858660833805638128 1] 22500192

[1 3551371861899513573 3551371861899513573 1] 7445771

[1 3271852081665507611 3271852081665507611 1] 284345

[1 5235884288566630025 5235884288566630025 1] 928922

[1 4559156434454441098 4559156434454441098 1] 17033346

[1 3338324673096263604 3338324673096263604 1] 13523519

[1 6513783888078605502 6513783888078605502 1] 11875307

[1 2810783061002379839 2810783061002379839 1] 5566211

[1 894503179976803868 894503179976803868 2] 9771195

[1 2239583862575887041 2239583862575887041 1] 5495122

[1 4852342352453309314 4852342352453309314 1] 12378825

[1 8861487328274733667 8861487328274733667 1] 4130239

[1 5199371706524493251 5199371706524493251 1] 3401038

[1 2337016797151025370 2337016797151025370 1] 17939391

[1 3210252149598671156 3210252149598671156 2] 15123626

[1 3526848735750231445 3526848735750231445 3] 10488085

[1 3523119062920112816 3523119062920112816 1] 7301207

[1 6418055994088674903 6418055994088674903 2] 19331930

[1 647491046073748595 647491046073748595 1] 2167752

[1 3289045874899206838 3289045874899206838 2] 15569450

[1 6451767927797156096 6451767927797156096 1] 13177135

[1 4509399839213241252 4509399839213241252 1] 18115335

[1 7758562614941667095 7758562614941667095 2] 11801847

[1 2331347999351084026 2331347999351084026 1] 2793334

[1 4377884285487400780 4377884285487400780 1] 455217

[1 5178792161997660435 5178792161997660435 1] 9254568

[1 7132460384727029320 7132460384727029320 2] 7520171

[1 4672142790945023815 4672142790945023815 1] 16836098

[1 8166414568124432591 8166414568124432591 1] 9916469

[1 5514366799865754161 5514366799865754161 1] 9342481

[1 130094826246450141 130094826246450141 1] 6846694

[1 3659813617944922327 3659813617944922327 2] 7109549

[1 2861478466821191741 2861478466821191741 1] 2488020

[1 9194213822884330834 9194213822884330834 1] 865750

[1 5063561934843515109 5063561934843515109 1] 7717192

[1 472640305114548655 472640305114548655 2] 17560097

[1 3236934473919397422 3236934473919397422 1] 13347711

[1 251690462219525063 251690462219525063 1] 19756283

[1 8746113330728608796 8746113330728608796 3] 6564652

[1 4073211198583022180 4073211198583022180 1] 1996423

[1 9194213822884330834 9194213822884330834 1] 1611964

[1 4412122026428717922 4412122026428717922 1] 15183301

[1 8685449636262271972 8685449636262271972 1] 17225570

[1 4219873354455086148 4219873354455086148 4] 13657805

[1 9111468288398707982 9111468288398707982 1] 9858638

[1 550672249647483098 550672249647483098 1] 9678928

[1 5199371706524493251 5199371706524493251 2] 2881153

[1 6513783888078605502 6513783888078605502 1] 9559179

[1 3558554399823816184 3558554399823816184 1] 9015444

[1 7856897504463192900 7856897504463192900 1] 3775545

[1 2491827283880295168 2491827283880295168 2] 15713973

[1 1242004612079023671 1242004612079023671 1] 5635118

[1 5416000030182068785 5416000030182068785 2] 6364425

[1 185118575590103668 185118575590103668 1] 11167374

[1 3271508910756835865 3271508910756835865 1] 4045326

[1 4122695951619468847 4122695951619468847 2] 11820199

[1 4827324001856983987 4827324001856983987 1] 7076918

[1 6274825479430247148 6274825479430247148 1] 14216012

[1 6170402025844769799 6170402025844769799 1] 325682

[1 8596722216914397364 8596722216914397364 1] 14542759

[1 7044189533465539850 7044189533465539850 1] 4481046

[1 4774416163461359252 4774416163461359252 3] 9456342

[1 914343859512707012 914343859512707012 1] 9425042

[1 9107500590212004521 9107500590212004521 2] 6723074

[1 2454684895762362559 2454684895762362559 1] 6935617

[1 2491827283880295168 2491827283880295168 1] 15205032

[1 415004406609487801 415004406609487801 1] 44096

[1 2454684895762362559 2454684895762362559 1] 7424390

[1 6397345207882921962 6397345207882921962 2] 12928509

[1 776057151823164264 776057151823164264 1] 4407339

[1 6468866080072798106 6468866080072798106 1] 12491229

[1 8123840614758380338 8123840614758380338 3] 545476

[1 9054274455774325283 9054274455774325283 2] 4630296

[1 4562440946473198944 4562440946473198944 3] 10090234

[1 3271508910756835865 3271508910756835865 2] 6675495

[1 6357559164249722615 6357559164249722615 4] 15248060

[1 319895222190797266 319895222190797266 1] 9388199

[1 6508099035400486672 6508099035400486672 1] 15043679

[1 2090043877226103601 2090043877226103601 2] 11220359

[1 6969888700525189384 6969888700525189384 1] 12353575

[1 8863376680030915354 8863376680030915354 1] 4045740

[1 3345713678998118409 3345713678998118409 1] 20765293

[1 7499126390569328487 7499126390569328487 1] 15184560

[1 1840298256139815746 1840298256139815746 1] 21656929

[1 1186413747289502661 1186413747289502661 2] 14924899

[1 7613436918011352707 7613436918011352707 1] 9762612

[1 5945104872396650355 5945104872396650355 1] 12031022

[1 6190378323326485826 6190378323326485826 1] 1350525

[1 579383808298780685 579383808298780685 1] 360037

[1 4238713034810074028 4238713034810074028 1] 17329035

[1 3829641242182598913 3829641242182598913 1] 3653161

[1 1929108437739011046 1929108437739011046 1] 9293329

[1 5460306810368422338 5460306810368422338 1] 16685697

[1 6423217256339162762 6423217256339162762 1] 12008716

[1 4956135701449413191 4956135701449413191 1] 3074886

[1 472640305114548655 472640305114548655 1] 19770664

[1 6041612706119483417 6041612706119483417 1] 9513147

[1 5416000030182068785 5416000030182068785 1] 10273257

[1 5724685807532549095 5724685807532549095 1] 16893448

[1 5885709345812136768 5885709345812136768 1] 12747721

[1 3582342302972989278 3582342302972989278 1] 9271420

[1 8454751433989030941 8454751433989030941 1] 10910069

[1 835059540630012492 835059540630012492 1] 16937084

[1 2059014550261616441 2059014550261616441 1] 6432100

[1 2046472722026058361 2046472722026058361 1] 5958497

[1 4073211198583022180 4073211198583022180 1] 1788730

[1 8223556651405343222 8223556651405343222 1] 4404404

[1 1890874750208477764 1890874750208477764 1] 23743931

[1 8639103653568590745 8639103653568590745 2] 9473433

[1 8123840614758380338 8123840614758380338 1] 2626264

[1 8456951483809752933 8456951483809752933 1] 22561218

[1 2838580124468335443 2838580124468335443 1] 792892

[1 962644554631157417 962644554631157417 1] 23021482

[1 222493149465179000 222493149465179000 2] 14034915

[1 7490339278880190554 7490339278880190554 2] 5050177

[1 5199371706524493251 5199371706524493251 1] 4904516

[1 2538156644782290352 2538156644782290352 2] 21862888

[1 5071069789944271384 5071069789944271384 1] 4701932

[1 1657028723439299735 1657028723439299735 2] 22840247

[1 2550598396595818528 2550598396595818528 1] 7281192

[1 7466460471292109233 7466460471292109233 1] 22439752

[1 6702312599032352784 6702312599032352784 2] 19001959

[1 9194213822884330834 9194213822884330834 1] 409517

[1 1176482845217077127 1176482845217077127 1] 19994969

[1 1790591590042843761 1790591590042843761 1] 11412232

[1 5199371706524493251 5199371706524493251 2] 3924920

[1 3964450872751765311 3964450872751765311 5] 8240634

[1 1217566094413633721 1217566094413633721 1] 882520

[1 6957926362045696369 6957926362045696369 1] 83012

[1 7979646042163024205 7979646042163024205 1] 3116748

[1 960478310206579209 960478310206579209 1] 3901877

[1 4194064741151001970 4194064741151001970 1] 6645396

[1 6872419454369207885 6872419454369207885 1] 18471115

[1 2681509236213133930 2681509236213133930 1] 20976864

[1 7316668006565156651 7316668006565156651 3] 10388923

[1 1994482117221562610 1994482117221562610 2] 20950101

[1 2681509236213133930 2681509236213133930 2] 23449803

[1 1572355345597119708 1572355345597119708 1] 4313548

[1 8630513118107105493 8630513118107105493 1] 21690962

[1 2704694954785049502 2704694954785049502 1] 23182399

[1 4898117658189580031 4898117658189580031 1] 3503179

[1 497283696040899058 497283696040899058 1] 22277879

[1 4086763913301779518 4086763913301779518 2] 3428611

[1 131118527416406417 131118527416406417 1] 18320918

[1 4841662169877694660 4841662169877694660 1] 27144786

[1 8178167647254100655 8178167647254100655 1] 15176713

[1 2247469374958062075 2247469374958062075 1] 1278535

[1 4073211198583022180 4073211198583022180 3] 290058

[1 7881522691004017370 7881522691004017370 1] 18918785

[1 2319188756839704936 2319188756839704936 1] 11124511

[1 130094826246450141 130094826246450141 1] 6544135

[1 8875211216651956425 8875211216651956425 2] 10174398

[1 1309962873612645872 1309962873612645872 3] 8953750

[1 3513591833870429566 3513591833870429566 1] 580464

[1 7523316024404873548 7523316024404873548 1] 6916905

[1 5609386143928750169 5609386143928750169 1] 2907918

[1 579383808298780685 579383808298780685 2] 1179499

[1 8457181607094899287 8457181607094899287 1] 6760700

[1 3460887766722519736 3460887766722519736 1] 22089652

[1 8380641326145083184 8380641326145083184 1] 2112193

[1 6679105948721570805 6679105948721570805 1] 1053020

[1 3152667199902966591 3152667199902966591 1] 608126

[1 5199641547698550621 5199641547698550621 1] 17532259

[1 2337016797151025370 2337016797151025370 1] 21647223

[1 1858959575435454377 1858959575435454377 2] 19290415

[1 789678821803831634 789678821803831634 1] 2644937

[1 8344753198079708132 8344753198079708132 1] 12682311

[1 2090043877226103601 2090043877226103601 1] 8280917

[1 3113311429710773546 3113311429710773546 1] 10393916

[1 3964450872751765311 3964450872751765311 1] 5039210

[1 4559156434454441098 4559156434454441098 1] 17163083

[1 794543592873094726 794543592873094726 1] 6967062

[1 4755877962832181671 4755877962832181671 2] 2995926

[1 8821009146333822433 8821009146333822433 1] 18262281

[1 6312818450892701566 6312818450892701566 2] 1504030

[1 3452460185093754177 3452460185093754177 1] 3919858

[1 2509139897261588936 2509139897261588936 1] 13075245

[1 3523119062920112816 3523119062920112816 3] 4085026

[1 6700757519863913125 6700757519863913125 2] 16774283

[1 5428676046622452730 5428676046622452730 1] 7260299

[1 1652415761709635407 1652415761709635407 1] 6143440

[1 5885251080520357233 5885251080520357233 1] 3526265

[1 2928647503566692007 2928647503566692007 1] 4060730

[1 8071399829518041455 8071399829518041455 1] 7097952

[1 6312818450892701566 6312818450892701566 2] 1634887

[1 3152846535939948506 3152846535939948506 2] 2964444

[1 4982225158532910574 4982225158532910574 1] 4340889

[1 5380935037227546951 5380935037227546951 1] 1110839

[1 2856539413314948307 2856539413314948307 1] 1884399

[1 9041943092826302859 9041943092826302859 1] 2839016

[1 6190378323326485826 6190378323326485826 3] 220793

[1 7465279672643454733 7465279672643454733 1] 1695484

[1 555990338574654921 555990338574654921 1] 6607738

[1 4365291373867262605 4365291373867262605 2] 7950526

[1 6324117110787121994 6324117110787121994 1] 7420013

[1 8382273392383741760 8382273392383741760 2] 6306733

[1 4811212750625984352 4811212750625984352 1] 990538

[1 4541849469276830559 4541849469276830559 1] 633423

[1 230278193608570785 230278193608570785 1] 198191

[1 6423217256339162762 6423217256339162762 1] 3478678

[1 7951556637785314359 7951556637785314359 2] 642187

[1 8796452180611775208 8796452180611775208 1] 1401572

[1 1190941672751438369 1190941672751438369 1] 1392376

[1 2036909659925042171 2036909659925042171 2] 1490888

[1 7008706794286656759 7008706794286656759 1] 1065359

[1 2334543312721676862 2334543312721676862 1] 4242875

[1 5788881540600095843 5788881540600095843 1] 3118474

[1 4021089274693363635 4021089274693363635 1] 4678720

[1 8674095322927264939 8674095322927264939 1] 2855523

[1 5010598880146546224 5010598880146546224 1] 1798047

[1 6041612706119483417 6041612706119483417 1] 2702715

[1 5152867666612939994 5152867666612939994 1] 381347

[1 3966236307845293463 3966236307845293463 1] 1449535

[1 8939596530689448133 8939596530689448133 1] 4133327

[1 3558554399823816184 3558554399823816184 1] 2880992

[1 769942351571177457 769942351571177457 1] 1117099

[1 4538011972252671285 4538011972252671285 2] 431894

[1 7496270305388725263 7496270305388725263 2] 1887404

[1 3575030713274475525 3575030713274475525 1] 4534055

[1 4826508407678470599 4826508407678470599 1] 289046

[1 3337160779789067394 3337160779789067394 2] 4164194

[1 7253081084140999038 7253081084140999038 1] 2891420

[1 2965510242296456540 2965510242296456540 1] 3724363

[1 7835094993739046534 7835094993739046534 1] 5511008

[1 5593717899833931073 5593717899833931073 1] 2753803

[1 1113918663186012118 1113918663186012118 1] 2516808

[1 3815681640050750010 3815681640050750010 1] 4996908

[1 1392740535184907307 1392740535184907307 1] 2643607

[1 1452942473505964474 1452942473505964474 1] 5066840

[1 4643234138915544813 4643234138915544813 1] 6895417

[1 3380263676397564338 3380263676397564338 1] 1248608

[1 3397674522525353132 3397674522525353132 3] 2347232

[1 4133824371882797777 4133824371882797777 1] 5918723

[1 8219298985896031870 8219298985896031870 1] 4148790

[1 407680228762628637 407680228762628637 1] 4264643

[1 5163588175456553285 5163588175456553285 1] 266931

[1 6702312599032352784 6702312599032352784 1] 6798287

[1 8228581028098359699 8228581028098359699 1] 3876427

[1 5041716788574141913 5041716788574141913 3] 4854506

[1 3203016569110680163 3203016569110680163 1] 6162188

[1 5718108348191035616 5718108348191035616 1] 5847866

[1 2269341530959424643 2269341530959424643 1] 4270184

[1 2367632123291455431 2367632123291455431 1] 6082105

[1 6520185239836403810 6520185239836403810 1] 1190518

[1 5873317982803155464 5873317982803155464 1] 5560620

[1 8555612586013675980 8555612586013675980 1] 3537259

[1 4486208703608854876 4486208703608854876 1] 3455753

[1 8323616056709401117 8323616056709401117 1] 5985906

[1 9194213822884330834 9194213822884330834 1] 727682

[1 4852342352453309314 4852342352453309314 1] 6311170

[1 3219269621666571012 3219269621666571012 1] 6470946

[1 2332873300355990351 2332873300355990351 3] 5019268

[1 1335048254029551544 1335048254029551544 1] 1636540

[1 5788881540600095843 5788881540600095843 1] 4690172

[1 5063561934843515109 5063561934843515109 1] 3972530

[1 1980691158095603428 1980691158095603428 2] 8501471

[1 8783398529814028400 8783398529814028400 1] 7663348

[1 2351868443412837382 2351868443412837382 2] 4352452

[1 3923278645798607949 3923278645798607949 1] 7804648

[1 3829641242182598913 3829641242182598913 2] 1940148

[1 550672249647483098 550672249647483098 2] 4333213

[1 8783398529814028400 8783398529814028400 2] 8350369

[1 5190854629269504638 5190854629269504638 1] 1126974

[1 6487420009656927523 6487420009656927523 2] 10224205

[1 2737049106520977071 2737049106520977071 1] 9871477

[1 6020890979708936693 6020890979708936693 1] 4072993

[1 2059014550261616441 2059014550261616441 1] 5388629

[1 5719856726457577661 5719856726457577661 4] 4623193

[1 230278193608570785 230278193608570785 1] 1100301

[1 4672142790945023815 4672142790945023815 1] 10620077

[1 5719717182571015391 5719717182571015391 1] 4951524

[1 8861487328274733667 8861487328274733667 1] 2081430

[1 7816894275143735653 7816894275143735653 1] 11405898

[1 8630513118107105493 8630513118107105493 1] 12673372

[1 14219070558469852 14219070558469852 1] 4057937

[1 3656344101082318758 3656344101082318758 1] 9355962

[1 7942517674964614188 7942517674964614188 1] 14184325

[1 2336099898871085227 2336099898871085227 1] 1020882

[1 4808101553268135358 4808101553268135358 1] 13163209

[1 282635039422144020 282635039422144020 1] 7608017

[1 7777977619431818208 7777977619431818208 1] 3974721

[1 8172922642733451540 8172922642733451540 1] 4292054

[1 8861487328274733667 8861487328274733667 1] 3699509

[1 8818877862451692445 8818877862451692445 1] 3162269

[1 3727930850643909137 3727930850643909137 1] 14673690

[1 8630513118107105493 8630513118107105493 1] 14661185

[1 3530022764642695888 3530022764642695888 1] 8069126

[1 6416684361604007311 6416684361604007311 1] 13058262

[1 6028182652995316019 6028182652995316019 1] 12527899

[1 5585786087383249728 5585786087383249728 1] 9843220

[1 7510830849849817936 7510830849849817936 1] 7480798

[1 6789022155914594207 6789022155914594207 1] 3450306

[1 7798246342830308968 7798246342830308968 1] 11103898

[1 8906311312446022368 8906311312446022368 2] 6207725

[1 6317955593750657091 6317955593750657091 1] 1995923

[1 5235884288566630025 5235884288566630025 1] 807205

[1 414355819421748683 414355819421748683 2] 14706684

[1 5475979248680422918 5475979248680422918 1] 15530355

[1 6812130681507053423 6812130681507053423 1] 16893119

[1 7999706091662848021 7999706091662848021 1] 2048458

[1 777597955150123734 777597955150123734 1] 6398200

[1 4002125427601191018 4002125427601191018 1] 12819456

[1 9223218755183230107 9223218755183230107 1] 10471972

[1 3674465782270809850 3674465782270809850 1] 6961097

[1 4973759262420791533 4973759262420791533 1] 425240

[1 8861487328274733667 8861487328274733667 1] 3000918

[1 4909640069524819720 4909640069524819720 1] 5687202

[1 3094667540105656806 3094667540105656806 1] 13139895

[1 1858959575435454377 1858959575435454377 1] 13415021

[1 4073211198583022180 4073211198583022180 1] 866197

[1 185177807332437299 185177807332437299 1] 16919521

[1 25808810848855699 25808810848855699 1] 14512100


[1 6332845802162722465 6332845802162722465 1] 2791668

[1 7496270305388725263 7496270305388725263 2] 7076507

[1 3815681640050750010 3815681640050750010 1] 10718361

[1 1779151639916505574 1779151639916505574 1] 13973598

[1 965512617529384234 965512617529384234 1] 8687915

[1 8579167014664097186 8579167014664097186 1] 16747984

[1 2090043877226103601 2090043877226103601 1] 12203696

[1 6190378323326485826 6190378323326485826 1] 796679

[1 241391968874552458 241391968874552458 1] 4754337

[1 3908090975176347760 3908090975176347760 2] 4304813

[1 855809072938529119 855809072938529119 1] 10601975

[1 4792288441176605021 4792288441176605021 2] 16975980

[1 3246353780858311150 3246353780858311150 1] 1314692

[1 3212214683265722945 3212214683265722945 2] 12453311

[1 5025943281449913462 5025943281449913462 1] 11556109

[1 3743210088908719144 3743210088908719144 1] 657955

[1 7218017804265400567 7218017804265400567 1] 167424

[1 2934363447031835758 2934363447031835758 1] 11157301

[1 9087915936908888447 9087915936908888447 2] 16802673

[1 2562215526203485763 2562215526203485763 1] 9706247

[1 894503179976803868 894503179976803868 1] 11006058

[1 5178122808022206607 5178122808022206607 1] 4494379

[1 522117222994015365 522117222994015365 1] 20243336

[1 8732085476929491618 8732085476929491618 1] 15900072

[1 8438789768457654395 8438789768457654395 1] 15671904

[1 3825613523896836328 3825613523896836328 1] 11959413

[1 6643227403971752890 6643227403971752890 1] 9087000

[1 6400049056656596056 6400049056656596056 1] 12788049

[1 345230284045436778 345230284045436778 2] 17183189

[1 2097449817658158751 2097449817658158751 1] 10283056

[1 472640305114548655 472640305114548655 1] 11632592

[1 2334543312721676862 2334543312721676862 1] 14240272

[1 5196006687241536103 5196006687241536103 3] 9055920

[1 3725094020181300213 3725094020181300213 1] 12799180



Prio stats: 394 85 17 3 1 0 0 0 0 0 0 0 0 0 0 0


Results: 100.00 9594045

**Results from the final ekxperiment:**

[1 5323113048839211569 5323113048839211569 1] 2897973

[1 5323113048839211569 5323113048839211569 1] 10214

[1 5199371706524493251 5199371706524493251 1] 1490733

[1 6061301652778360708 6061301652778360708 1] 3141493

[1 3659813617944922327 3659813617944922327 1] 2242418

[1 6551056528233317404 6551056528233317404 1] 4019283

[1 5152867666612939994 5152867666612939994 1] 316505

[1 4306388032135015083 4306388032135015083 1] 2690537

[1 3582342302972989278 3582342302972989278 2] 2382878

[1 5152867666612939994 5152867666612939994 2] 7987

[1 6551056528233317404 6551056528233317404 1] 4116705

[1 8123840614758380338 8123840614758380338 1] 471414

[1 14219070558469852 14219070558469852 1] 1834478

[1 5199371706524493251 5199371706524493251 2] 5538

[1 6299299371226330111 6299299371226330111 1] 949388

[1 8545497753826994372 8545497753826994372 1] 5210627

[1 5199371706524493251 5199371706524493251 1] 4286

[1 14219070558469852 14219070558469852 1] 5119

[1 4210581282492980127 4210581282492980127 2] 4099319

[1 5226070023190972674 5226070023190972674 1] 19640

[1 2681509236213133930 2681509236213133930 1] 5889660

[1 3371221844736955655 3371221844736955655 1] 4010871

[1 2681509236213133930 2681509236213133930 1] 6939

[1 2417774981122755451 2417774981122755451 1] 6416503

[1 7751036336550852449 7751036336550852449 2] 2471695

[1 2417774981122755451 2417774981122755451 1] 5826541

[1 5665786584277501498 5665786584277501498 1] 5026299

[1 4711316578468217750 4711316578468217750 1] 1607323

[1 5323113048839211569 5323113048839211569 1] 14535

[1 443797163739200304 443797163739200304 1] 4230208

[1 419959660935379333 419959660935379333 1] 1354678

[1 4439128167132747160 4439128167132747160 1] 2019385

[1 4411586658559271441 4411586658559271441 2] 6872231

[1 5199371706524493251 5199371706524493251 1] 5510

[1 3219269621666571012 3219269621666571012 1] 3982586

[1 428285387533163711 428285387533163711 1] 287147

[1 3661979449103474211 3661979449103474211 1] 1869255

[1 6508099035400486672 6508099035400486672 3] 4082605

[1 4587930096661733970 4587930096661733970 1] 2548979

[1 676565140536873255 676565140536873255 1] 2039921

[1 7479665055322049871 7479665055322049871 1] 3545121

[1 7465279672643454733 7465279672643454733 1] 1883927

[1 4096930159418457413 4096930159418457413 2] 699109

[1 6676147598389536847 6676147598389536847 1] 1615941

[1 7758562614941667095 7758562614941667095 1] 3419000

[1 7542673075594086020 7542673075594086020 1] 1636744

[1 3380263676397564338 3380263676397564338 2] 522908

[1 14219070558469852 14219070558469852 1] 2805

[1 5358995021737652853 5358995021737652853 2] 4287600

[1 8447424885188984022 8447424885188984022 1] 1556480

[1 419959660935379333 419959660935379333 1] 3177

[1 8765615669523681939 8765615669523681939 1] 1521384

[1 4418986465783198893 4418986465783198893 1] 2291181

[1 5054306381671161295 5054306381671161295 2] 3542361

[1 4711016277695541463 4711016277695541463 1] 3330726

[1 3168228909583649578 3168228909583649578 1] 4412870

[1 3300549992339425025 3300549992339425025 1] 3975773

[1 8576653471017792763 8576653471017792763 1] 2812843

[1 6312818450892701566 6312818450892701566 2] 853418

[1 3277991424207011103 3277991424207011103 1] 923688

[1 5358995021737652853 5358995021737652853 1] 23435

[1 4640306689896283532 4640306689896283532 1] 5842957

[1 4675313619226882656 4675313619226882656 2] 3799766

[1 8630513118107105493 8630513118107105493 2] 6635534

[1 6769470164041216243 6769470164041216243 2] 1753757

[1 4313074964976661539 4313074964976661539 1] 4411126

[1 3073303311992054111 3073303311992054111 1] 8150943

[1 5727926382741058057 5727926382741058057 1] 5820147

[1 5235884288566630025 5235884288566630025 1] 529191

[1 4672142790945023815 4672142790945023815 1] 9459526

[1 5609386143928750169 5609386143928750169 1] 1722045

[1 5199371706524493251 5199371706524493251 1] 4053

[1 2408144213467734955 2408144213467734955 2] 6777580

[1 1537871660103321085 1537871660103321085 1] 7004737

[1 8106418617074936444 8106418617074936444 2] 6687029

[1 8861487328274733667 8861487328274733667 1] 2340071

[1 7751036336550852449 7751036336550852449 1] 4534

[1 7263920461483667833 7263920461483667833 3] 131202

[1 8178167647254100655 8178167647254100655 1] 7073785

[1 4852342352453309314 4852342352453309314 1] 8670462

[1 8545497753826994372 8545497753826994372 2] 5997

[1 6935269914745579775 6935269914745579775 2] 8481349

[1 7258030776582813733 7258030776582813733 2] 8693202

[1 8545497753826994372 8545497753826994372 1] 9741

[1 6733148917571626198 6733148917571626198 1] 3012609

[1 2702469276737005753 2702469276737005753 1] 6759316

[1 6418055994088674903 6418055994088674903 2] 9302655

[1 697023732095936426 697023732095936426 1] 8214111

[1 6621806134722217994 6621806134722217994 3] 3689036


[1 1999070770062910591 1999070770062910591 1] 2098477

[1 5899104248299270511 5899104248299270511 1] 5023872

[1 1858959575435454377 1858959575435454377 1] 8752926

[1 3980262377739853399 3980262377739853399 1] 2159344

[1 5046249221331440250 5046249221331440250 1] 3684205

[1 2906895808741546301 2906895808741546301 1] 7826037

[1 3679629128133838277 3679629128133838277 1] 380080

[1 3980262377739853399 3980262377739853399 2] 5757

[1 4829750008164147974 4829750008164147974 1] 240839

[1 1559190386264860990 1559190386264860990 1] 11182699

[1 3837879182316570790 3837879182316570790 1] 3668303

[1 4755769266264313978 4755769266264313978 1] 5838884

[1 1924345543027782702 1924345543027782702 1] 6034719

[1 1726119651011849087 1726119651011849087 1] 8835093

[1 8025119966655469405 8025119966655469405 1] 2923848

[1 3380263676397564338 3380263676397564338 1] 10058

[1 4134066680141017990 4134066680141017990 1] 870915

[1 1657028723439299735 1657028723439299735 1] 9631718

[1 6383064505001044642 6383064505001044642 1] 3865599

[1 6661929279557956789 6661929279557956789 1] 3898283

[1 2740874517211224045 2740874517211224045 1] 14210601

[1 366035088203681309 366035088203681309 1] 9494681

[1 7885990956964797791 7885990956964797791 1] 4240266

[1 472640305114548655 472640305114548655 1] 10679091

[1 2237646372472201809 2237646372472201809 2] 3776064

[1 1943465972204786521 1943465972204786521 1] 10208424

[1 1494610507527821925 1494610507527821925 1] 5642787

[1 7000092202700241638 7000092202700241638 1] 5077201

[1 472640305114548655 472640305114548655 1] 7626

[1 6418055994088674903 6418055994088674903 2] 16922

[1 4210581282492980127 4210581282492980127 1] 8088

[1 9087915936908888447 9087915936908888447 1] 13715715

[1 777597955150123734 777597955150123734 1] 5839930

[1 2881907375742268536 2881907375742268536 2] 10279084

[1 62078986177768511 62078986177768511 1] 1442170

[1 5222133264898256115 5222133264898256115 2] 7218280

[1 4389097367932351452 4389097367932351452 1] 4490508

[1 7021863040962118426 7021863040962118426 1] 15065219

[1 4934221230837358068 4934221230837358068 1] 6368944

[1 3837879182316570790 3837879182316570790 1] 8964

[1 2404331784854299912 2404331784854299912 1] 2178119

[1 5486430009089896035 5486430009089896035 1] 3581045

[1 4049776256710809090 4049776256710809090 1] 12017239

[1 5235884288566630025 5235884288566630025 1] 8298

[1 171574233629385747 171574233629385747 1] 2600718

[1 7094574829709000416 7094574829709000416 2] 13094188

[1 3523119062920112816 3523119062920112816 1] 5428887

[1 4858660833805638128 4858660833805638128 1] 7685180

[1 3224496798420488480 3224496798420488480 1] 12873969

[1 78272782157371108 78272782157371108 1] 14408639

[1 4002065833995603862 4002065833995603862 1] 6089890

[1 9041943092826302859 9041943092826302859 1] 4698851

[1 5190854629269504638 5190854629269504638 1] 589137

[1 8773876334263565533 8773876334263565533 1] 1923799

[1 2562215526203485763 2562215526203485763 2] 3836838

[1 348341910997470776 348341910997470776 1] 240138

[1 101175050661003722 101175050661003722 1] 7898904

[1 4570918472550733256 4570918472550733256 1] 5901146

[1 1217082174760581124 1217082174760581124 1] 8083490

[1 3778255577798444672 3778255577798444672 2] 7573728

[1 6418055994088674903 6418055994088674903 2] 13550

[1 6334889449731842403 6334889449731842403 1] 6173355

[1 6696084871028655697 6696084871028655697 1] 2589094

[1 3338324673096263604 3338324673096263604 1] 3630572

[1 4852342352453309314 4852342352453309314 1] 10252

[1 4567303845284399364 4567303845284399364 1] 2543773

[1 4516712816156734168 4516712816156734168 1] 7479512

[1 3551371861899513573 3551371861899513573 1] 2735256

[1 4559156434454441098 4559156434454441098 1] 10524389

[1 647491046073748595 647491046073748595 1] 1280830

[1 894503179976803868 894503179976803868 2] 2890269

[1 7012966272631410635 7012966272631410635 1] 568573

[1 2337016797151025370 2337016797151025370 1] 7593347

[1 3210252149598671156 3210252149598671156 2] 4667880

[1 6513783888078605502 6513783888078605502 1] 7384218

[1 4672142790945023815 4672142790945023815 1] 3944

[1 2810783061002379839 2810783061002379839 1] 1976282

[1 5235884288566630025 5235884288566630025 1] 3998

[1 3526848735750231445 3526848735750231445 3] 2999185

[1 3271852081665507611 3271852081665507611 1] 277883

[1 3289045874899206838 3289045874899206838 2] 5296302

[1 2239583862575887041 2239583862575887041 1] 1694676

[1 3523119062920112816 3523119062920112816 1] 4111

[1 7758562614941667095 7758562614941667095 2] 3171

[1 4509399839213241252 4509399839213241252 1] 6883117

[1 6451767927797156096 6451767927797156096 1] 3839874

[1 8861487328274733667 8861487328274733667 1] 3748

[1 472640305114548655 472640305114548655 2] 3925

[1 5199371706524493251 5199371706524493251 1] 7625

[1 5178792161997660435 5178792161997660435 1] 2482677

[1 251690462219525063 251690462219525063 1] 5532932

[1 7132460384727029320 7132460384727029320 2] 2367802

[1 8166414568124432591 8166414568124432591 1] 3598217

[1 647491046073748595 647491046073748595 1] 4208

[1 5514366799865754161 5514366799865754161 1] 3112379

[1 3659813617944922327 3659813617944922327 2] 3473

[1 8685449636262271972 8685449636262271972 1] 6350149

[1 2331347999351084026 2331347999351084026 1] 1549590

[1 130094826246450141 130094826246450141 1] 2561309

[1 3236934473919397422 3236934473919397422 1] 4468085

[1 4219873354455086148 4219873354455086148 4] 3707455

[1 4377884285487400780 4377884285487400780 1] 117464

[1 4412122026428717922 4412122026428717922 1] 6539921

[1 6513783888078605502 6513783888078605502 1] 11530

[1 5063561934843515109 5063561934843515109 1] 2574791

[1 9111468288398707982 9111468288398707982 1] 2346523

[1 550672249647483098 550672249647483098 1] 2240224

[1 2861478466821191741 2861478466821191741 1] 840679

[1 8746113330728608796 8746113330728608796 3] 1715096

[1 2491827283880295168 2491827283880295168 2] 5448546

[1 9194213822884330834 9194213822884330834 1] 357243

[1 3558554399823816184 3558554399823816184 1] 2535843

[1 4073211198583022180 4073211198583022180 1] 376222

[1 9194213822884330834 9194213822884330834 1] 4595

[1 5199371706524493251 5199371706524493251 2] 4375

[1 6274825479430247148 6274825479430247148 1] 4442305

[1 185118575590103668 185118575590103668 1] 4006372

[1 4122695951619468847 4122695951619468847 2] 3587160

[1 7856897504463192900 7856897504463192900 1] 1363529

[1 2491827283880295168 2491827283880295168 1] 17300

[1 8596722216914397364 8596722216914397364 1] 4906932

[1 5416000030182068785 5416000030182068785 2] 2557932

[1 1242004612079023671 1242004612079023671 1] 2152145

[1 4827324001856983987 4827324001856983987 1] 2589321

[1 3271508910756835865 3271508910756835865 1] 1655351

[1 4774416163461359252 4774416163461359252 3] 3164652

[1 914343859512707012 914343859512707012 1] 2968448

[1 6508099035400486672 6508099035400486672 1] 73889

[1 6170402025844769799 6170402025844769799 1] 169914

[1 9107500590212004521 9107500590212004521 2] 3188371

[1 7044189533465539850 7044189533465539850 1] 2833249

[1 6397345207882921962 6397345207882921962 2] 5724857

[1 6357559164249722615 6357559164249722615 4] 4399712

[1 2454684895762362559 2454684895762362559 1] 2869237

[1 3345713678998118409 3345713678998118409 1] 9038608

[1 2454684895762362559 2454684895762362559 1] 3054888

[1 6468866080072798106 6468866080072798106 1] 7678097

[1 1840298256139815746 1840298256139815746 1] 9703652

[1 472640305114548655 472640305114548655 1] 7978

[1 776057151823164264 776057151823164264 1] 1780535

[1 3271508910756835865 3271508910756835865 2] 6717

[1 4562440946473198944 4562440946473198944 3] 5723265

[1 7499126390569328487 7499126390569328487 1] 7838525

[1 415004406609487801 415004406609487801 1] 50837

[1 8123840614758380338 8123840614758380338 3] 3664

[1 9054274455774325283 9054274455774325283 2] 2206975

[1 319895222190797266 319895222190797266 1] 5714150

[1 1186413747289502661 1186413747289502661 2] 6353535

[1 2090043877226103601 2090043877226103601 2] 6976632

[1 6969888700525189384 6969888700525189384 1] 8882321

[1 8863376680030915354 8863376680030915354 1] 1098323

[1 4238713034810074028 4238713034810074028 1] 6822542

[1 5945104872396650355 5945104872396650355 1] 3439437

[1 5460306810368422338 5460306810368422338 1] 5935902

[1 5724685807532549095 5724685807532549095 1] 5183947

[1 5416000030182068785 5416000030182068785 1] 14988

[1 5885709345812136768 5885709345812136768 1] 2939707

[1 7613436918011352707 7613436918011352707 1] 5408500

[1 1657028723439299735 1657028723439299735 2] 606822

[1 1929108437739011046 1929108437739011046 1] 4229715

[1 6041612706119483417 6041612706119483417 1] 2822814

[1 3582342302972989278 3582342302972989278 1] 7519

[1 3829641242182598913 3829641242182598913 1] 1508977

[1 6190378323326485826 6190378323326485826 1] 954458

[1 6423217256339162762 6423217256339162762 1] 9686183

[1 579383808298780685 579383808298780685 1] 414348

[1 1890874750208477764 1890874750208477764 1] 11577581

[1 4956135701449413191 4956135701449413191 1] 1595228

[1 8456951483809752933 8456951483809752933 1] 9067318

[1 835059540630012492 835059540630012492 1] 10424227

[1 8454751433989030941 8454751433989030941 1] 5110568

[1 962644554631157417 962644554631157417 1] 9023538

[1 7466460471292109233 7466460471292109233 1] 5982703

[1 2681509236213133930 2681509236213133930 2] 11184

[1 2059014550261616441 2059014550261616441 1] 3008766

[1 2681509236213133930 2681509236213133930 1] 4470

[1 2538156644782290352 2538156644782290352 2] 9433671

[1 2046472722026058361 2046472722026058361 1] 2730266

[1 8639103653568590745 8639103653568590745 2] 3555303

[1 222493149465179000 222493149465179000 2] 5327601

[1 8630513118107105493 8630513118107105493 1] 7282

[1 6702312599032352784 6702312599032352784 2] 8428044

[1 1176482845217077127 1176482845217077127 1] 9107571

[1 8223556651405343222 8223556651405343222 1] 1673578

[1 4073211198583022180 4073211198583022180 1] 4862

[1 8123840614758380338 8123840614758380338 1] 5357

[1 5199371706524493251 5199371706524493251 1] 14616

[1 1994482117221562610 1994482117221562610 2] 6689268

[1 1790591590042843761 1790591590042843761 1] 3316399

[1 6872419454369207885 6872419454369207885 1] 6608201

[1 7490339278880190554 7490339278880190554 2] 1894226

[1 2704694954785049502 2704694954785049502 1] 6754977

[1 4841662169877694660 4841662169877694660 1] 9062709

[1 5071069789944271384 5071069789944271384 1] 1559548

[1 2550598396595818528 2550598396595818528 1] 2969714

[1 2838580124468335443 2838580124468335443 1] 123491

[1 497283696040899058 497283696040899058 1] 6818665

[1 3964450872751765311 3964450872751765311 5] 1511094

[1 8178167647254100655 8178167647254100655 1] 9387

[1 7316668006565156651 7316668006565156651 3] 2030769

[1 131118527416406417 131118527416406417 1] 4805616

[1 5199371706524493251 5199371706524493251 2] 4405

[1 2337016797151025370 2337016797151025370 1] 3487

[1 4194064741151001970 4194064741151001970 1] 1759485

[1 9194213822884330834 9194213822884330834 1] 4010


[1 7881522691004017370 7881522691004017370 1] 4503615

[1 960478310206579209 960478310206579209 1] 1027429

[1 1858959575435454377 1858959575435454377 2] 2536

[1 7979646042163024205 7979646042163024205 1] 703708

[1 3460887766722519736 3460887766722519736 1] 5027185

[1 1217566094413633721 1217566094413633721 1] 251614

[1 6957926362045696369 6957926362045696369 1] 93192

[1 1572355345597119708 1572355345597119708 1] 1044912

[1 4559156434454441098 4559156434454441098 1] 3901

[1 2319188756839704936 2319188756839704936 1] 2846609

[1 5199641547698550621 5199641547698550621 1] 4313122

[1 8875211216651956425 8875211216651956425 2] 3089561

[1 4898117658189580031 4898117658189580031 1] 927195

[1 130094826246450141 130094826246450141 1] 3780

[1 1309962873612645872 1309962873612645872 3] 2209197

[1 4086763913301779518 4086763913301779518 2] 1445798

[1 8821009146333822433 8821009146333822433 1] 5250219

[1 2247469374958062075 2247469374958062075 1] 773769

[1 8457181607094899287 8457181607094899287 1] 1671484

[1 4073211198583022180 4073211198583022180 3] 6638

[1 7523316024404873548 7523316024404873548 1] 3017747

[1 2090043877226103601 2090043877226103601 1] 13679

[1 8344753198079708132 8344753198079708132 1] 4918886

[1 5609386143928750169 5609386143928750169 1] 2879

[1 6700757519863913125 6700757519863913125 2] 5226351

[1 3513591833870429566 3513591833870429566 1] 272768

[1 3113311429710773546 3113311429710773546 1] 3505685

[1 579383808298780685 579383808298780685 2] 3304

[1 8380641326145083184 8380641326145083184 1] 562467

[1 3964450872751765311 3964450872751765311 1] 4456

[1 794543592873094726 794543592873094726 1] 1990048

[1 2509139897261588936 2509139897261588936 1] 4578890

[1 6679105948721570805 6679105948721570805 1] 604391

[1 3152667199902966591 3152667199902966591 1] 7765

[1 789678821803831634 789678821803831634 1] 1149889

[1 4755877962832181671 4755877962832181671 2] 1182297

[1 3523119062920112816 3523119062920112816 3] 5224

[1 3452460185093754177 3452460185093754177 1] 1351908

[1 5428676046622452730 5428676046622452730 1] 3156978

[1 6312818450892701566 6312818450892701566 2] 2962

[1 1652415761709635407 1652415761709635407 1] 3365082

[1 8071399829518041455 8071399829518041455 1] 2886684

[1 5885251080520357233 5885251080520357233 1] 1857802

[1 2928647503566692007 2928647503566692007 1] 2492895

[1 6312818450892701566 6312818450892701566 2] 38686

[1 9041943092826302859 9041943092826302859 1] 10873

[1 3152846535939948506 3152846535939948506 2] 1626600

[1 4365291373867262605 4365291373867262605 2] 4727082

[1 4982225158532910574 4982225158532910574 1] 3263700

[1 8382273392383741760 8382273392383741760 2] 3489180

[1 7465279672643454733 7465279672643454733 1] 4281

[1 2856539413314948307 2856539413314948307 1] 1350053

[1 5380935037227546951 5380935037227546951 1] 779770

[1 555990338574654921 555990338574654921 1] 5172261

[1 6324117110787121994 6324117110787121994 1] 5788680

[1 6423217256339162762 6423217256339162762 1] 2951

[1 6190378323326485826 6190378323326485826 3] 3006

[1 4811212750625984352 4811212750625984352 1] 1010858

[1 4541849469276830559 4541849469276830559 1] 679090

[1 230278193608570785 230278193608570785 1] 215833

[1 7951556637785314359 7951556637785314359 2] 724188

[1 8796452180611775208 8796452180611775208 1] 1529159

[1 1190941672751438369 1190941672751438369 1] 1457720

[1 2036909659925042171 2036909659925042171 2] 1656937

[1 7008706794286656759 7008706794286656759 1] 1696772

[1 6041612706119483417 6041612706119483417 1] 4422

[1 2334543312721676862 2334543312721676862 1] 4592148

[1 3558554399823816184 3558554399823816184 1] 8428

[1 5788881540600095843 5788881540600095843 1] 4421371

[1 4021089274693363635 4021089274693363635 1] 5047720

[1 8674095322927264939 8674095322927264939 1] 2785957

[1 5010598880146546224 5010598880146546224 1] 1395910

[1 5152867666612939994 5152867666612939994 1] 3441

[1 3966236307845293463 3966236307845293463 1] 1455688

[1 8939596530689448133 8939596530689448133 1] 4873676

[1 769942351571177457 769942351571177457 1] 1286664

[1 4538011972252671285 4538011972252671285 2] 472243

[1 7496270305388725263 7496270305388725263 2] 2064350

[1 4826508407678470599 4826508407678470599 1] 164284

[1 3575030713274475525 3575030713274475525 1] 5263512

[1 3337160779789067394 3337160779789067394 2] 4986206

[1 6702312599032352784 6702312599032352784 1] 37027

[1 2965510242296456540 2965510242296456540 1] 5225483

[1 7253081084140999038 7253081084140999038 1] 6271996

[1 5593717899833931073 5593717899833931073 1] 3516297

[1 1113918663186012118 1113918663186012118 1] 2688516

[1 7835094993739046534 7835094993739046534 1] 8131803

[1 3380263676397564338 3380263676397564338 1] 4602

[1 4643234138915544813 4643234138915544813 1] 7015448

[1 1392740535184907307 1392740535184907307 1] 3884791

[1 3397674522525353132 3397674522525353132 3] 2346711

[1 3815681640050750010 3815681640050750010 1] 7947081

[1 8219298985896031870 8219298985896031870 1] 4351300

[1 1452942473505964474 1452942473505964474 1] 7550946

[1 5163588175456553285 5163588175456553285 1] 527435

[1 407680228762628637 407680228762628637 1] 5482663

[1 8228581028098359699 8228581028098359699 1] 4169298

[1 4133824371882797777 4133824371882797777 1] 8772706

[1 4852342352453309314 4852342352453309314 1] 4909

[1 5041716788574141913 5041716788574141913 3] 4650804

[1 3219269621666571012 3219269621666571012 1] 4879

[1 2269341530959424643 2269341530959424643 1] 3116405

[1 3203016569110680163 3203016569110680163 1] 7260822

[1 8323616056709401117 8323616056709401117 1] 4098858

[1 5873317982803155464 5873317982803155464 1] 5417726

[1 2367632123291455431 2367632123291455431 1] 6684439

[1 5788881540600095843 5788881540600095843 1] 74556

[1 6520185239836403810 6520185239836403810 1] 1315811

[1 5718108348191035616 5718108348191035616 1] 8079099

[1 9194213822884330834 9194213822884330834 1] 3567

[1 8555612586013675980 8555612586013675980 1] 4031200

[1 4486208703608854876 4486208703608854876 1] 3728386

[1 5063561934843515109 5063561934843515109 1] 4276

[1 2332873300355990351 2332873300355990351 3] 2992959

[1 1980691158095603428 1980691158095603428 2] 5769914

[1 1335048254029551544 1335048254029551544 1] 1160844

[1 8783398529814028400 8783398529814028400 1] 3690673

[1 4672142790945023815 4672142790945023815 1] 4549

[1 550672249647483098 550672249647483098 2] 7488

[1 8783398529814028400 8783398529814028400 2] 3583170

[1 8630513118107105493 8630513118107105493 1] 7854

[1 3923278645798607949 3923278645798607949 1] 5117914

[1 2351868443412837382 2351868443412837382 2] 2609641

[1 3829641242182598913 3829641242182598913 2] 4344

[1 6487420009656927523 6487420009656927523 2] 4515670

[1 2059014550261616441 2059014550261616441 1] 3800

[1 2737049106520977071 2737049106520977071 1] 5883107

[1 5190854629269504638 5190854629269504638 1] 36092

[1 7816894275143735653 7816894275143735653 1] 5003744

[1 6020890979708936693 6020890979708936693 1] 2339252

[1 5719856726457577661 5719856726457577661 4] 1924296

[1 5719717182571015391 5719717182571015391 1] 1737170

[1 8630513118107105493 8630513118107105493 1] 31699

[1 4808101553268135358 4808101553268135358 1] 4817624

[1 230278193608570785 230278193608570785 1] 15625

[1 7942517674964614188 7942517674964614188 1] 6165815

[1 8861487328274733667 8861487328274733667 1] 3359

[1 3656344101082318758 3656344101082318758 1] 3365858

[1 14219070558469852 14219070558469852 1] 605333

[1 3727930850643909137 3727930850643909137 1] 4911172

[1 282635039422144020 282635039422144020 1] 3696803

[1 2336099898871085227 2336099898871085227 1] 497495

[1 6416684361604007311 6416684361604007311 1] 6705432

[1 6028182652995316019 6028182652995316019 1] 6364701

[1 7777977619431818208 7777977619431818208 1] 1763768

[1 8861487328274733667 8861487328274733667 1] 10658

[1 5475979248680422918 5475979248680422918 1] 6290745

[1 8172922642733451540 8172922642733451540 1] 1189000

[1 414355819421748683 414355819421748683 2] 6121425

[1 3530022764642695888 3530022764642695888 1] 5421586

[1 8818877862451692445 8818877862451692445 1] 1821627

[1 6812130681507053423 6812130681507053423 1] 7752041

[1 7510830849849817936 7510830849849817936 1] 4255332

[1 5585786087383249728 5585786087383249728 1] 7449580

[1 7798246342830308968 7798246342830308968 1] 8152488

[1 8906311312446022368 8906311312446022368 2] 2878147


[1 6789022155914594207 6789022155914594207 1] 1464511

[1 1858959575435454377 1858959575435454377 1] 3959

[1 777597955150123734 777597955150123734 1] 13759

[1 4002125427601191018 4002125427601191018 1] 4598769

[1 6317955593750657091 6317955593750657091 1] 543756

[1 9223218755183230107 9223218755183230107 1] 4092020

[1 5235884288566630025 5235884288566630025 1] 3578

[1 3094667540105656806 3094667540105656806 1] 5042616

[1 185177807332437299 185177807332437299 1] 6404617

[1 25808810848855699 25808810848855699 1] 4419439

[1 3674465782270809850 3674465782270809850 1] 2729513

[1 7999706091662848021 7999706091662848021 1] 762451

[1 3815681640050750010 3815681640050750010 1] 4433

[1 2090043877226103601 2090043877226103601 1] 13296

[1 8579167014664097186 8579167014664097186 1] 5244833

[1 4909640069524819720 4909640069524819720 1] 1803298

[1 8861487328274733667 8861487328274733667 1] 5253

[1 7496270305388725263 7496270305388725263 2] 3938

[1 1779151639916505574 1779151639916505574 1] 4267527

[1 4792288441176605021 4792288441176605021 2] 5116488

[1 4973759262420791533 4973759262420791533 1] 271625

[1 965512617529384234 965512617529384234 1] 2513451

[1 9087915936908888447 9087915936908888447 2] 111571

[1 4073211198583022180 4073211198583022180 1] 7630

[1 6332845802162722465 6332845802162722465 1] 1082662

[1 855809072938529119 855809072938529119 1] 3029843

[1 522117222994015365 522117222994015365 1] 5166229

[1 3212214683265722945 3212214683265722945 2] 3442386

[1 241391968874552458 241391968874552458 1] 2147575

[1 894503179976803868 894503179976803868 1] 16590

[1 5025943281449913462 5025943281449913462 1] 3728798

[1 6190378323326485826 6190378323326485826 1] 8486

[1 3908090975176347760 3908090975176347760 2] 1841801

[1 8732085476929491618 8732085476929491618 1] 4932587

[1 2562215526203485763 2562215526203485763 1] 8659

[1 2334543312721676862 2334543312721676862 1] 10196

[1 8438789768457654395 8438789768457654395 1] 5378896

[1 345230284045436778 345230284045436778 2] 5496807

[1 472640305114548655 472640305114548655 1] 6527

[1 3246353780858311150 3246353780858311150 1] 811165

[1 2934363447031835758 2934363447031835758 1] 5334951

[1 6400049056656596056 6400049056656596056 1] 4945251

[1 3743210088908719144 3743210088908719144 1] 214895

[1 7218017804265400567 7218017804265400567 1] 276747

[1 5178122808022206607 5178122808022206607 1] 2123770

[1 3825613523896836328 3825613523896836328 1] 5849738

[1 6643227403971752890 6643227403971752890 1] 4203579

[1 2097449817658158751 2097449817658158751 1] 5268810

[1 3725094020181300213 3725094020181300213 1] 6411742

[1 5196006687241536103 5196006687241536103 3] 4054778



Prio stats: 394 85 17 3 1 0 0 0 0 0 0 0 0 0 0 0

Results: 100.00 4046274





