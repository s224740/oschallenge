#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>
#include <sys/resource.h>
#include <openssl/sha.h>
#include "messages.h"

// Cache size
#define CACHE_CAPACITY 3000

// Position in the cache
int cache_position = 1;

// Structure to hold cache items (key-hash pairs)
struct CacheEntry {
    uint8_t hash_value[32];
    uint64_t found_key;
};

// Cache array to store key-hash pairs
struct CacheEntry* CacheArray[CACHE_CAPACITY];

// Global socket descriptors for graceful termination
int server_socket;
int client_socket;

// Signal handler for graceful termination
void handle_signal(int signal) {
    close(server_socket);
    close(client_socket);
    exit(0);
}

// Function to insert key-hash pair into the cache
void cache_insert(uint64_t key, uint8_t* hash_value) {
    // Allocate memory for a new cache item
    struct CacheEntry *entry = (struct CacheEntry*) malloc(sizeof(struct CacheEntry));
    memcpy(entry->hash_value, hash_value, 32);
    entry->found_key = key;

    // Find an empty slot in the cache array
    int cache_index = 1;
    while (CacheArray[cache_index] != NULL && cache_index != CACHE_CAPACITY) {
        ++cache_index; // Move to the next slot
    }

    // Store the new cache entry
    CacheArray[cache_index] = entry;
    ++cache_position; // Update the position for the next insert
}

// Function to search for a hash in the cache
struct CacheEntry* cache_search(uint8_t* hash_value) {
    // Loop through the cache to find the matching hash
    for (int index = 1; index < cache_position; index++) {
        if (CacheArray[index] != NULL && memcmp(CacheArray[index]->hash_value, hash_value, 32) == 0) {
            return CacheArray[index]; // Return the matching cache entry
        }
    }
    return NULL; // Return NULL if no match is found
}

// Handles client requests to find a key matching the given hash (multithreaded)
void* processClientRequest(void *socketDescriptor) {
    // Extract the socket descriptor from the pointer and free the allocated memory
    int client_socket = *(int*)socketDescriptor;
    free(socketDescriptor);

    // Buffer to read the request
    uint8_t request_buffer[PACKET_REQUEST_SIZE];
    read(client_socket, request_buffer, PACKET_REQUEST_SIZE);

    // Extract the hash, range, and priority from the request
    uint8_t request_hash[32];
    uint64_t range_start;
    uint64_t range_end;
    uint8_t priority;
    memcpy(request_hash, request_buffer + PACKET_REQUEST_HASH_OFFSET, 32);
    memcpy(&range_start, request_buffer + PACKET_REQUEST_START_OFFSET, 8);
    memcpy(&range_end, request_buffer + PACKET_REQUEST_END_OFFSET, 8);
    memcpy(&priority, request_buffer + PACKET_REQUEST_PRIO_OFFSET, 1);

    // Sets process priority based on priority level
    setpriority(PRIO_PROCESS, 0, 15 - priority);

    // Convert the byte order from network to host
    range_start = htobe64(range_start);
    range_end = htobe64(range_end);

    // Search the cache for a pre-computed hash
    struct CacheEntry* cached_result = cache_search(request_hash);
    uint64_t found_key;
    uint8_t calculated_hash[32];

    if (cached_result == NULL) {
        // If not found in cache, compute the hash for the range
        for (found_key = range_start; found_key < range_end; found_key++) {
            SHA256((uint8_t *)&found_key, 8, calculated_hash);
            if (memcmp(request_hash, calculated_hash, 32) == 0)
                break; // Found the matching key
        }
        // Insert the new key-hash pair into the cache
        cache_insert(found_key, calculated_hash);
    } else {
        // If found in the cache, use the cached key
        found_key = cached_result->found_key;
    }

    // Sending the found key back to the client
    found_key = be64toh(found_key);
    write(client_socket, &found_key, 8);

    // Terminate the connection
    close(client_socket);
    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    // Register signal handler
    signal(SIGINT, handle_signal);

    // Create server socket
    server_socket = socket(AF_INET, SOCK_STREAM, 0);

    // Enable address reuse
    if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int)) < 0) {
        perror("setsockopt(SO_REUSEADDR) failed");
        exit(1);
    }

    // Configure server address
    struct sockaddr_in server_address;
    memset(&server_address, 0, sizeof(server_address));
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = INADDR_ANY;
    server_address.sin_port = htons(atoi(argv[1]));

    // Bind socket to address
    if (bind(server_socket, (struct sockaddr *)&server_address, sizeof(server_address)) < 0) {
        perror("ERROR on binding");
        exit(1);
    }

    // Start listening for connections
    listen(server_socket, 100);
    printf("Server is listening on port %s\n", argv[1]);

    // Client address structure
    struct sockaddr_in client_address;
    int client_address_length = sizeof(client_address);

    // Main loop to accept and handle connections
    while (1) {
        // Accept a new connection
        client_socket = accept(server_socket, (struct sockaddr *)&client_address, &client_address_length);
        if (client_socket < 0) {
            perror("ERROR on accept");
            exit(1);
        }
        printf("Client connected\n");

        // Allocate memory for the client socket descriptor
        int *client_socket_ptr = malloc(sizeof(int));
        memcpy(client_socket_ptr, &client_socket, sizeof(int));

        // Start a new thread to handle the client request
        pthread_t client_thread;
        pthread_create(&client_thread, NULL, processClientRequest, client_socket_ptr);
    }

    return 0;
}
